<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateKey extends Model
{
    protected $fillable = [
        'key',
        'date'
    ];
}
