Instrukcja instalacji:

1) Ściągamy repozytorium na dysk za pomocą komendy:
git clone https://michal1311b@bitbucket.org/michal1311b/teb_task_backend.git

2) Kopiujemy plik konfiguracyjny .env.example i zapisujemy jako .env

3) Dodajemy połączenie do bazy danych MYSQL

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=teb_task
DB_USERNAME=root
DB_PASSWORD=

4) Dodajemy klucz aplikacji za pomocą polecenia
php artisan key:generate

5) Instalujemy zależności composer za pomocą instrukcji:
na środowisku Windows: composer install
na środowisku Linux: composer.phar install

6) po instalacji uruchamiamy polecenia
php artisan cache:clear
php artisan config:clear

7) uruchamiamy projekt za pomocą polecenia
php artisan serve --port=8008

8) uruchamiamy migracje za pomocą polecenia:
php artisan migrate