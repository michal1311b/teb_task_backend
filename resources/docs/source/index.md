---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_dbc201bc1abea4dc5a1b63017b63bb82 -->
## api/get-states
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/get-states" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/get-states"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "name": "Wielkopolskie",
    "slug": "wielkopolskie"
}
```
> Example response (404):

```json
null
```

### HTTP Request
`GET api/get-states`


<!-- END_dbc201bc1abea4dc5a1b63017b63bb82 -->

<!-- START_aa1182a06c10faba850915d2f6e9d7c9 -->
## api/store-keys
> Example request:

```bash
curl -X POST \
    "http://localhost/api/store-keys" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"body":[]}'

```

```javascript
const url = new URL(
    "http://localhost/api/store-keys"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "body": []
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`POST api/store-keys`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `body` | array |  optional  | with dates and keys
    
<!-- END_aa1182a06c10faba850915d2f6e9d7c9 -->


