<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * @response 200 {
     *  "name": "Wielkopolskie",
     *  "slug": "wielkopolskie",
     *  "created_at": "2020-04-11 20:40:00",
     *  "updated_at": "2020-04-11 20:40:00"
     * }
     * @response 404 {
     *  "message": "No query results for model [\App\State]"
     * }
     */
    public function getStates()
    {
        $states = State::all();

        return $states->toArray();
    }
}
