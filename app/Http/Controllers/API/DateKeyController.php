<?php

namespace App\Http\Controllers\API;

use App\DateKey;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class DateKeyController extends Controller
{
    /**
     * @bodyParam body array with dates and keys
     * @response {
     *   "status": 201,
     *   "message": "Udało się zapisać klucze"
     *  }
     *  @response with errors {
     *   "status": HTTP response code,
     *   "message": "HTTP response message wtih errors"
     *  }
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            foreach($request->body as $body) {
                DateKey::create([
                    'key' => $body[0],
                    'date' => $body[1]
                ]);
            }
            DB::commit();

            return response()->json([
                'status' => 201,
                'message' => 'Udało się zapisać klucze'
            ]);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollback();

            return response()->json([
                'status' => $e->getCode(),
                'message' => $e->getMessage()
            ]);
        }
    }
}
